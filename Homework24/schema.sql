-- создание таблицы Продукт
create table product
(
    -- идентификатор строки, всегда уникальный, генерируется базой данных для таблицы Продукт
    id          serial primary key,
     -- определяем столбцы
    description varchar(100), -- Описание товара
    price       varchar(10),  -- Его цена
    count       integer check ( count > 0) -- количество
);

-- добавление информации для таблицы Продукты
insert into product(description, price, count) values ('Samsung','12_000',3);
insert into product(description, price, count) values ('Nokia','9_000',5);
insert into product(description, price, count) values ('Digma','5_000',4);
insert into product(description, price, count) values ('Iphone','30_000',10);
insert into product(description, price, count) values ('Samsung A21','11_500',1);

-- создание таблицы Заказчик
create table customer
(
    -- идентификатор строки, всегда уникальный, генерируется базой данных для таблицы Заказчик
    id              serial primary key,
    first_last_name varchar(100) -- имя/фамилия заказчика
);

-- добавление информации для таблицы Заказчик
insert into customer(first_last_name) values ('Иван Иванов');
insert into customer(first_last_name) values ('Федор Федоров');
insert into customer(first_last_name) values ('Дима Петрушев');
insert into customer(first_last_name) values ('Стас Михайлов');


-- создание таблицы Заказ
create table check_order
(
    -- идентификатор строки, всегда уникальный, генерируется базой данных для таблицы заказ
    id       serial primary key,
    owner_id_product integer,
    -- внешний ключ из таблицы продукт
    foreign key (owner_id_product) references product (id),
    owner_id_customer integer,
    -- внешний ключ из таблицы заказчик
    foreign key (owner_id_customer) references customer (id),
    order_data date, -- дата заказа
    count_product integer check ( count_product > 0 ) -- количество товара
);

-- добавление информации для таблицы Заказы
insert into check_order(owner_id_product, owner_id_customer, order_data, count_product) values (5,3,date(now()),1);
insert into check_order(owner_id_product, owner_id_customer, order_data, count_product) values (1,1,date(now()),2);
insert into check_order(owner_id_product, owner_id_customer, order_data, count_product) values (3,4,date(now()),3);
insert into check_order(owner_id_product, owner_id_customer, order_data, count_product) values (4,2,date(now()),1);


-- получение всех данных из таблицы Заказы
select *
from check_order;

-- обновление информации
update check_order
set owner_id_product = 2
where id = 1;


-- получить внешний ключ из таблицы продукт и количество с сортировкой по убиванию количества(товара)
select owner_id_product, count_product
from check_order
order by count_product desc;

-- получить наименования продукта
select *
from product
where price > '8_000';



-- получить имена пользователей  и количество его заказов
select first_last_name, (select count(*) from check_order where owner_id_customer = customer.id) as order_count
from customer;

-- получить имена пользователей, который купил Iphone
select first_last_name
from customer
where customer.id in
      (select product.id from product where description = 'Iphone');

-- все заказчики, и заказы у которых есть заказчики
select *
from customer a
         left join check_order c on a.id = c.owner_id_customer;

-- все заказы, и заказчики у которых есть заказы
select *
from customer a
         right join check_order c on a.id = c.owner_id_product;

-- только заказы и заказчики, которые есть друг у друга
select *
from customer a
         inner join check_order c on a.id = c.owner_id_product;

-- все заказчики, все заказы
select *
from customer a
         full join check_order c on a.id = c.owner_id_customer;