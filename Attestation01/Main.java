package ru.pavlenko_homeworks;

public class Main {

    public static void main(String[] args) {
    	//Создаем репозиторий из файла
    	UsersRepositoryFileImpl usersRep=new UsersRepositoryFileImpl("userslist.txt");

    	//Выводим репозиторий
    	System.out.println(usersRep.toString());

    	//Редактируем репозиторий
    	usersRep.repositoryEdit();

		//Выводим отредактированый репозиторий
    	System.out.println(usersRep.toString());

    	//Перезаписываем репозиторий
		usersRep.update();
    }
}

