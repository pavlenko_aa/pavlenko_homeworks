package ru.pavlenko_homeworks;

import java.io.*;
import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;

public class UsersRepositoryFileImpl {

    public static final String MSG_LOADING ="\n Данные загружены из файла ";
    public static final String SPLITER_FOR_STRING_IN_FILE ="\\|";
    public static final String MSG_UPLOADING ="\n Данные перезаписаны в файл ";
    public static final String STRING_SPLITER ="|";
    public static final String TO_STRING_MSG1 = "\n\n (Список всех пользователей из файла - ";
    public static final String TO_STRING_MSG1_FINISH = ")\n";
    public static final String TO_STRING_HORIZONTAL_SPLITER = "*"+"\n\n";
    public static final String REPOSITORE_EDIT_QUESTION1 ="Хотите внеси изменения в пользователей?(1-Да/2-Нет)";
    public static final String REPOSITORE_EDIT_MSG1 ="Выберите ID пользователя для редактирования";
    public static final String REPOSITORE_EDIT_MSG2 ="Выберите 1 - Да или 2-Нет";
    public static final String REPOSITORE_EDIT_MSG3 ="Введите верный ID пользователя или -1 для завершения редактирования данного пользователя";
    public static final String REPOSITORE_EDIT_MSG4 ="Редактирование репозитория завершено";

    //Имя файла репозитория
    private final String repositoryFileName;

    //HashMap множество для хранения данных о пользователях
    private HashMap<Integer,User> usersRepository=new HashMap<>();

    /**
     * Конструктор репозитория
     * @param filename файл с данными о пользователях
     */
    public UsersRepositoryFileImpl(String filename) {
        this.repositoryFileName = filename;
        loadRepository(repositoryFileName);
    }

    /**
     * Создает HashMap множества из файла формата
     * ID|Имя|Возраст|Признак трудоустройства
     * @param repositoryFileName - файл из которого будет создан репозиторий
     */
    private void loadRepository(String repositoryFileName){
        try {
            BufferedReader usersFromFile = new BufferedReader(new FileReader(repositoryFileName));

            //Преобразовываем stream строк в HashMap где ключ ID, значение - объект типа User
            this.usersRepository=
                    (HashMap<Integer, User>) usersFromFile.lines()
                    .map(arg->arg.split(SPLITER_FOR_STRING_IN_FILE))
                    .map(arg-> new User(
                            Integer.parseInt(arg[0])
                            , arg[1]
                            , Integer.parseInt(arg[2])
                            , Boolean.parseBoolean(arg[3])))
                    .collect(Collectors.toMap(User::getID, arg->arg,(arg1, arg2)->arg1));

        }catch (IOException e ){
            System.out.println(e.getMessage());
            throw new IllegalArgumentException(e);
        }
        System.out.println(MSG_LOADING+repositoryFileName);
    }

    /**
     * Записывает обновление в репозиторий
     */
    public void update(){
        try {
            BufferedWriter usersToFile = new BufferedWriter(new FileWriter(repositoryFileName));
            for (Integer id:usersRepository.keySet()) {
                //Каждую пару Ключ-Значение преобразуем в строку для записи и записываем
                usersToFile.write(makeStringForUpdate(id));
                usersToFile.newLine();
                usersToFile.flush();
            }
        }catch (IOException e ){
            System.out.println(e.getMessage());
            throw new IllegalArgumentException(e);
        }
        System.out.println(MSG_UPLOADING+repositoryFileName);
    }

    /**
     * Возвращает пользователя с указанным ID
     * @param id уникальный номер пользователя
     * @return объект типа User
     */
    public User findById(int id){
        return usersRepository.get(id);
    }

    /**
     * Создает из HashMap элемента строку для записи в файл
     * @param id ID элемента
     * @return форматированая строка для записи в файл
     */
    public String makeStringForUpdate(int id){

        return id +
                STRING_SPLITER + this.usersRepository.get(id).getUserName() +
                STRING_SPLITER + this.usersRepository.get(id).getUserAge() +
                STRING_SPLITER + this.usersRepository.get(id).getWorker();
    }

    /**
     * Переопределена для формирования форматированного списка из HashMap
     * @return Возвращает форматированный список
     */
    @Override
    public String toString(){
        StringBuilder allUsersInString=new StringBuilder()
                .append(TO_STRING_MSG1)
                .append(repositoryFileName)
                .append(TO_STRING_MSG1_FINISH)
                .append(TO_STRING_HORIZONTAL_SPLITER);

        Set<Integer> allKeys = usersRepository.keySet();
        for (Integer key:allKeys){
            allUsersInString
                    .append(usersRepository.get(key).toString());
        }
        return allUsersInString.toString();
    }

    public void repositoryEdit(){
        //Текущий редактируемый пользователь
        User userForEdit;
        //Переменная содержащая значение 1 если идет редактирование и 2 если редактирование прекращено
        int isEdit;
        //ID для поиска пользователя которого нужно отредектировать
        int idForEdit;


        System.out.println(REPOSITORE_EDIT_QUESTION1);
        isEdit=ServisMetods.inputInt();
        // Будет запрашивать ввод с консоли пока не будет введено значение 1 или 2
        while (!(isEdit==1||isEdit==2)){
            System.out.println(REPOSITORE_EDIT_MSG2);
            isEdit=ServisMetods.inputInt();
        }
        // Выполняется пока идет редактирование
        while (isEdit==1){
            System.out.println(this.toString());

            //переменная для отслеживания не хочет ли пользователь прервать редактирование конкретного пользователя
            boolean breakEdit=false;

            System.out.println(REPOSITORE_EDIT_MSG1);
            idForEdit=ServisMetods.inputInt();

            //Считываем пользователя по введенному ID
            userForEdit=this.findById(idForEdit);

            //Выполняется пока не будет введен корректный ID или принудительно прекращено редактирование пользователя
            while ((userForEdit==null&&!breakEdit)){
                System.out.println(REPOSITORE_EDIT_MSG3);
                idForEdit=ServisMetods.inputInt();
                if (idForEdit==-1){
                    breakEdit=true;
                }
                userForEdit=this.findById(idForEdit);
            }

            //Если id пользователя введен верно редактируем пользователя
            if(!breakEdit){
                userForEdit.userEdit();

            }

            //Запрашиваем хотим ли отредактировать другого пользователя
            System.out.println(REPOSITORE_EDIT_QUESTION1);
            isEdit=ServisMetods.inputInt();
            while (!(isEdit==1||isEdit==2)){
                System.out.println(REPOSITORE_EDIT_MSG2);
                isEdit=ServisMetods.inputInt();
            }
        }
        System.out.println(REPOSITORE_EDIT_MSG4);
    }

}
