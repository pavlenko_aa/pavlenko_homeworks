package ru.pavlenko_homeworks;

/**
 * @author Pavlenko Artem
 */
import java.util.Scanner;

public class ServisMetods {

    public static final String ASK_MSG="Введите число";

    public static int inputInt (){

        Scanner scanner=new Scanner(System.in);
        String selectString = scanner.nextLine();

        boolean correctInput=false;
        int selection=0;

        while (!correctInput){
            try {
                selection=Integer.parseInt(selectString);
            }catch (NumberFormatException e){
                System.out.println(ASK_MSG);
                selectString = scanner.nextLine();
                continue;
            }
            correctInput=true;
        }
        return selection;
    }

}
