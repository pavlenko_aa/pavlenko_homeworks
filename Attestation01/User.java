package ru.pavlenko_homeworks;

import java.util.Scanner;

public class User {
    //Строковые константы
    public static final String TO_STRING_START = "[ ";
    public static final String TO_STRING_FINISH = " ]\n";
    public static final String TO_STRING_SPACE = " ";
    public static final String TO_STRING_SPLITER = "|";
    public static final String TO_STRING_AGE_MSG = " возраст ";
    public static final String TO_STRING_IS_WORKER = " Трудоустроен";
    public static final String TO_STRING_ISNOT_WORKER = " Безработный ";

    public static final String EDIT_USER_MSG1 = "\n" + "Редактируется пользователь";
    public static final String EDIT_USER_MSG2 = "Выберите:";
    public static final String EDIT_USER_MSG3 = "Редактирование завершено";
    public static final String EDIT_USER_MSG4 = "выберите верный вариант";
    public static final String EDIT_USER_MSG5 = "Изменить имя с ";
    public static final String EDIT_USER_MSG6 = "Изменить возраст с ";
    public static final String EDIT_USER_MSG7 = "Статус трудоустройства изменен с ";
    public static final String EDIT_USER_MSG8 = "Трудоустроен(а) на  Безработный";
    public static final String EDIT_USER_MSG9 = "Безработный(ая) на Трудоустроен(а)";
    public static final String EDIT_USER_SPLITER_TO = " на ";
    public static final String EDIT_USER_MSG_SPLITER = "\n";
    public static final String EDIT_USER_SELECT_MSG1 = "1 - для редактирования Имени";
    public static final String EDIT_USER_SELECT_MSG2 = "2 - для редактирования Возраста";
    public static final String EDIT_USER_SELECT_MSG3 = "3 - для редактирования Трудоустройства";
    public static final String EDIT_USER_SELECT_MSG4 = "4 - для завершения редактирования";

    private String userName;
    private Integer userAge;
    private Boolean isWorker;
    private final Integer ID;

    public User(int id, String userName, int userAge, Boolean isWorker) {
        this.userName = userName;
        this.userAge = userAge;
        this.isWorker = isWorker;
        this.ID = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserAge() {
        return userAge;
    }

    public void setUserAge(int userAge) {
        this.userAge = userAge;
    }

    public Boolean getWorker() {
        return isWorker;
    }

    public void setWorker(Boolean worker) {
        isWorker = worker;
    }

    public int getID() {
        return ID;
    }

    /**
     * Переопределена для вывода информации о пользователе в строке
     *
     * @return форматированая строка информации о пользователе
     */
    @Override
    public String toString() {
        StringBuilder user = new StringBuilder();
        user
                .append(TO_STRING_START)
                .append(this.ID)
                .append(TO_STRING_SPACE)
                .append(getUserName())
                .append(TO_STRING_SPACE)
                .append(TO_STRING_SPLITER + TO_STRING_AGE_MSG)
                .append(this.userAge)
                .append(TO_STRING_SPACE)
                .append(TO_STRING_SPLITER);


        if (getWorker()) {
            user.append(TO_STRING_IS_WORKER + TO_STRING_FINISH);
        } else {
            user.append(TO_STRING_ISNOT_WORKER + TO_STRING_FINISH);
        }
        return user.toString();
    }

    /**
     * Редактирование пользователя
     */
    public void userEdit() {
        Scanner scanner = new Scanner(System.in);
        int selection = -1;
        while (selection != 4) {

            System.out.println(EDIT_USER_MSG1);
            System.out.println(this.toString());

            System.out.println(EDIT_USER_MSG2
                    + EDIT_USER_MSG_SPLITER + EDIT_USER_SELECT_MSG1
                    + EDIT_USER_MSG_SPLITER + EDIT_USER_SELECT_MSG2
                    + EDIT_USER_MSG_SPLITER + EDIT_USER_SELECT_MSG3
                    + EDIT_USER_MSG_SPLITER + EDIT_USER_SELECT_MSG4
            );
            //Счттываем значение с консоли и обрабатываем его
            selection = ServisMetods.inputInt();

            //Если введен неверный вариант запрашиваем заново
            if (selection > 4 || selection < 1) {
                System.out.println(EDIT_USER_MSG4);
                continue;
            }

            if (selection == 1) {
                System.out.print(EDIT_USER_MSG5 + this.getUserName() + EDIT_USER_SPLITER_TO);
                String newName = scanner.next();
                System.out.println();
                this.setUserName(newName);
            }

            if (selection == 2) {
                System.out.print(EDIT_USER_MSG6 + this.getUserAge() + EDIT_USER_SPLITER_TO);
                this.setUserAge(ServisMetods.inputInt());
                System.out.println();
            }

            if (selection == 3) {
                System.out.print(EDIT_USER_MSG7);
                if (this.getWorker()) {
                    System.out.print(EDIT_USER_MSG8);
                } else {
                    System.out.print(EDIT_USER_MSG9);
                }
                this.setWorker(!this.getWorker());
                System.out.println();
            }

            if (selection == 4) {
                System.out.println(EDIT_USER_MSG3);
                System.out.println(this.toString());
            }
        }
    }
}
