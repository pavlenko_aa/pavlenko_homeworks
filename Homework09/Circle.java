package ru.pavlenkoaa;

/**
 * @author Pavlenko Artem
 */
public class Circle extends Ellipse {


    public Circle(double radius) {
        super(radius, radius);
    }

    public double getPerimeter(double radius) {

        return (2 * Math.PI * radius);
    }


}
