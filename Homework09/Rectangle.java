package ru.pavlenkoaa;

/**
 * @author Pavlenko Artem
 */
public class Rectangle extends Figure {


    public Rectangle(double a, double b) {
        super(a, b);

    }

    public double getPerimeter(double a,double b) {

        return (2 * (a + b));
    }

}
