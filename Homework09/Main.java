package ru.pavlenkoaa;

public class Main {

    public static void main(String[] args) {
        Figure figure = new Figure(1,3);
        Rectangle rectangle = new Rectangle(1,3);
        Ellipse ellipse = new Ellipse(2,3);
        Circle circle = new Circle(2);
        Square square = new Square(3);
        System.out.println(figure.getPerimeter());
        System.out.println(rectangle.getPerimeter(1,3));
        System.out.println(ellipse.getPerimeter(2,3));
        System.out.println(circle.getPerimeter(2));
        System.out.println(square.getPerimeter(3));

    }
}
