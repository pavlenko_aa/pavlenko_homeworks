package ru.pavlenkoaa;

/**
 * @author Pavlenko Artem
 */
public class Square extends Rectangle {

    public Square(double a) {
        super(a,a);

    }

    public double getPerimeter(double a) {

        return (4 * a);
    }
}
