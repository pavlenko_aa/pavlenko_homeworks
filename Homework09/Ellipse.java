package ru.pavlenkoaa;

/**
 * @author Pavlenko Artem
 */
public class Ellipse extends Figure {


    public Ellipse(double a, double b) {
        super(a, b);
    }

    public double getPerimeter(double a, double b) {

        return 4 * (Math.PI * a * b + (a - b)) / a + b;
    }

}
