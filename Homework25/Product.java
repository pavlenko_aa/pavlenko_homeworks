package ru.pcs;

import lombok.*;

/**
 * @author Pavlenko Artem
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {
    private int id;
    private String description;
    private double price;
    private int count_products;
}
