package ru.pcs;

import java.util.List;

/**
 * @author Pavlenko Artem
 */
public interface ProductRepository {
    List<Product> findAllProduct();

    List<Product> findAllByPrice(double price);

    void save(Product product);

}
