create table product
(
    id             serial primary key,
    description    varchar(100),
    price         double precision check ( price > 0 ),
    count_products integer check ( count_products > 0 )
);

create table customer
(
    id         serial primary key,
    first_last_name varchar(50)

);

insert into customer(first_last_name) values ('Иван П');
insert into customer(first_last_name) values ('Степан Ф');
insert into customer(first_last_name) values ('Илья М');
insert into customer(first_last_name) values ('Павел П');
insert into customer(first_last_name) values ('Мария С');
insert into customer(first_last_name) values ('Семен А');
insert into customer(first_last_name) values ('Вячеслав Ш');
insert into customer(first_last_name) values ('Марина А');
insert into customer(first_last_name) values ('Галина Д');
insert into customer(first_last_name) values ('Алексей Я');


create table check_order
(
    owner_id_product integer,
    foreign key (owner_id_product) references product(id),
    owner_id_customer integer,
    foreign key (owner_id_customer) references customer(id),
    data_order date,
    count integer check ( count > 0 )
);

insert into check_order(owner_id_product, owner_id_customer, count,data_order) values (1,2,1,'21-07-2021');
insert into check_order(owner_id_product, owner_id_customer, count,data_order) values (3,2,4,'21-07-2021');
insert into check_order(owner_id_product, owner_id_customer, count,data_order) values (2,3,5,'21-07-2021');
insert into check_order(owner_id_product, owner_id_customer, count,data_order) values (4,1,4,'21-07-2021');
insert into check_order(owner_id_product, owner_id_customer, count,data_order) values (1,4,6,'22-07-2021');
insert into check_order(owner_id_product, owner_id_customer, count,data_order) values (6,2,8,'22-07-2021');
insert into check_order(owner_id_product, owner_id_customer, count,data_order) values (7,5,1,'22-07-2021');
insert into check_order(owner_id_product, owner_id_customer, count,data_order) values (8,2,22,'23-07-2021');
insert into check_order(owner_id_product, owner_id_customer, count,data_order) values (3,4,5,'23-07-2021');
insert into check_order(owner_id_product, owner_id_customer, count,data_order) values (9,5,1,'23-07-2021');
insert into check_order(owner_id_product, owner_id_customer, count,data_order) values (10,2,3,'24-07-2021');