package ru.pcs;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

/**
 * @author Pavlenko Artem
 */
public class Main {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/pcs_2",
                "postgres", "QAZxsw123");

        ProductRepository productRepository = new ProductRepositoryJdbcTemplateImpl(dataSource);

        //Результат работы реализаций

        System.out.println(productRepository.findAllProduct());
        System.out.println("-----------------------------------");
        System.out.println(productRepository.findAllByPrice(400));



    }
}
