package ru.pcs;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

/**
 * @author Pavlenko Artem
 */
public class ProductRepositoryJdbcTemplateImpl implements ProductRepository {

    // language = SQL
    private static final String SQL_INSERT = "insert into product(description, price, count_products) values(?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from product order by id";


    private JdbcTemplate jdbcTemplate;

    public ProductRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String description = row.getString("description");
        double price = row.getDouble("price");
        int count_products = row.getInt("count_products");

        return new Product(id, description, price, count_products);
    };

    /**
     * Возвращает весь список товаров
     *
     * @return Список товаров
     */

    @Override
    public List<Product> findAllProduct() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    /**
     * Возвращает все записи о товаре с учетом заданной цены
     *
     * @param price цена единицы товара
     * @return список объектов Item
     */

    private String sqlByPrice(double price) {
        String SQL_SELECT_PRICE = new String();
        SQL_SELECT_PRICE = "select * from product where price > " + price;
        return SQL_SELECT_PRICE;
    }


    @Override
    public List<Product> findAllByPrice(double price) {
        String SQL_SELECT_PRICE = sqlByPrice(price);
        return jdbcTemplate.query(SQL_SELECT_PRICE, productRowMapper);
    }

    /**
     * Записывает данные о товаре в таблицу
     *
     * @param product объект с содержанием информации о товаре
     */

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getDescription(), product.getPrice(), product.getCount_products());
    }
}
