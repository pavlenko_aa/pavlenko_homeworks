package ru.pavlenkoaa;

/**
 * @author Pavlenko Artem
 */
public class Square extends Rectangle implements Movable {

    public Square(double a) {
        super(a,a);
        this.setXCenter(0);
        this.setYCenter(0);
    }

    public double getPerimeter(double a) {

        return (4 * a);
    }

    @Override
    public void move(double x, double y) {
        setXCenter(x);
        setYCenter(y);
        System.out.println("Изменение координат в интерфейсе Перемещаемый, для Квадрата: " + "x: " + x + " , " + " y: " + y);
    }
}
