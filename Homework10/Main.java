package ru.pavlenkoaa;

public class Main {

    public static void main(String[] args) {
        Figure figure = new Figure(1,3);
        Rectangle rectangle = new Rectangle(1,3);
        Ellipse ellipse = new Ellipse(2,3);
        Circle circle = new Circle(2);
        Square square = new Square(3);

        Movable[] figures = new Movable[]{circle, square};
        figures[0].move(2,3);
        figures[1].move(2,3);

        System.out.println("Perimeter rectangle " + rectangle.getPerimeter(1,3));
        System.out.println("Perimeter ellipse " + ellipse.getPerimeter(2,3));
        System.out.println("Perimeter circle " + circle.getPerimeter(2));
        System.out.println("Perimeter square " + square.getPerimeter(3));

    }
}
