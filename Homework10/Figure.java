package ru.pavlenkoaa;

/**
 * @author Pavlenko Artem
 */
public class Figure {

    private double x,y;
    private double xCenter,yCenter;

    public void setXCenter(double xCenter) {
        this.xCenter = xCenter;
    }

    public void setYCenter(double yCenter) {
        this.yCenter = yCenter;
    }

    public Figure(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getPerimeter() {
        return 0;
    }
}
