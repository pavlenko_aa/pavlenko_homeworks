package ru.pavlenkoaa;

/**
 * @author Pavlenko Artem
 */
public class Circle extends Ellipse implements Movable {


    public Circle(double radius) {
        super(radius, radius);
        this.setXCenter(0);
        this.setYCenter(0);
    }

    public double getPerimeter(double radius) {

        return (2 * Math.PI * radius);
    }


    @Override
    public void move(double x, double y) {
        setXCenter(x);
        setYCenter(y);
        System.out.println("Изменение координат в интерфейсе Перемещаемый, для Круга: " + "x: " + x + " , " + " y: " + y);
    }
}
