package ru.pavlenko_homeworks;

/**
 * @author Pavlenko Artem
 */
public class Car {
   private String number;
   private String model;
   private String color;
   private Long mileAge;
   private Long price;

    public Car(String number, String model, String color, long mileAge, long price) {
        this.number = number;
        this.model = model;
        this.color = color;
        this.mileAge = mileAge;
        this.price = price;
    }

    public String getNumber() {
        return number;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public long getMileAge() {
        return mileAge;
    }

    public long getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "number='" + number + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", mileAge=" + mileAge +
                ", price=" + price +
                '}';
    }
}
