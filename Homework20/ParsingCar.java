package ru.pavlenko_homeworks;

import java.nio.file.*;
import java.io.IOException;
import java.util.Comparator;
import java.util.function.Function;


/**
 * @author Pavlenko Artem
 */
public class ParsingCar {

    private Path file = Paths.get("cars.txt");

    private Function<String, Car> fileParsing = line -> {
        String[] array = line.split("\\|");
        return new Car(array[0], array[1], array[2],
                Long.parseLong(array[3]), Long.parseLong(array[4]));

    };
    public void parsingCar() {
        try {
            Files.lines(file)
                    .map(fileParsing)
                    .filter(car -> car.getColor().equals("Черный") || car.getMileAge() == 0)
                    .forEach(car -> System.out.println("Номера всех автомобилей, имеющих черный " +
                            "цвет или нулевой пробег. = " + car.getNumber()));


            Long countUniqueCars = Files.lines(file)
                    .map(fileParsing)
                    .filter(car -> car.getPrice() >= 700000L && car.getPrice() <= 800000L)
                    .map(car -> car.getModel()).distinct().count();
            System.out.println("\nКоличество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. = "
                    + countUniqueCars);


            String colorCar = Files.lines(file)
                    .map(fileParsing)
                    .min(Comparator.comparingLong(Car::getPrice)).get().getColor();
            System.out.println("\nЦвет автомобиля с минимальной стоимостью. = "
                    + colorCar);


            Double averagePrice = Files.lines(file)
                    .map(fileParsing)
                    .filter(car -> car.getModel().equals("Toyota_Camry"))
                    .mapToLong(Car::getPrice).average().getAsDouble();
            System.out.println("\nСреднюю стоимость Toyota Camry. = "
                    + averagePrice);

        } catch (IOException e) {
            throw new IllegalArgumentException(e);

        }
    }
}

