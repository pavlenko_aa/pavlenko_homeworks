package ru.pavlenko_homework19;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        List<User> users = usersRepository.findAll();

        for (User user : users) {
            System.out.println("имя: " + user.getName() + "   возраст: " + user.getAge() + "   статус о работе :" + user.isWorker());
        }

        User useSave = new User("Александр",32,true);
        usersRepository.save(useSave);
        System.out.println();
        
        
        List<User> usersByAge = usersRepository.findByAge(23);
        for (User user : usersByAge){
            System.out.println(user);
        }
        System.out.println();
        
        
        List<User> usersByIsWorker = usersRepository.findByIsWorkerIsTrue();
        for (User user : usersByIsWorker){
            System.out.println(user);
        }
    }
}
