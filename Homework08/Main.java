
public class Main {

    public static void main(String[] args) {
        Human[] humans = new Human[10];
        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human("Безымянный №" + i +" - " + "Вес", (float) (Math.random() * 100f));
        }
        sortWeight(humans);
        for (Human human : humans){
            System.out.println(human.getName() + " " + human.getWeight() + " ");
        }
    }
    public static void sortWeight(Human[] humans){
        for (int i = 0;i < humans.length -1;i ++){
            int minWeight = i;
            for(int j = i;j < humans.length;j++){
                if(humans[j].getWeight() < humans[minWeight].getWeight()){
                    minWeight = j;
                }
            }
            Human temp = humans[i];
            humans[i] = humans[minWeight];
            humans[minWeight] = temp;
        }
    }
}


