/**
 * @author Pavlenko Artem
 */
public class Human {
    private String name;
    private float weight;

    public Human(String name, float weight) {
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public float getWeight() {
        return weight;
    }
}

