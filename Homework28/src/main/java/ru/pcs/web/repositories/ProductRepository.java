package ru.pcs.web.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.pcs.web.models.Product;

import javax.sql.DataSource;
import java.util.List;


/**
 * @author Pavlenko Artem
 */

@Component
public class ProductRepository implements ShopClothingServices {

    // language = SQL
    private static final String SQL_INSERT = "INSERT INTO products(category, title, price, count_product) VALUES (?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_ALL = "SELECT * FROM products ORDER BY id";


    private final JdbcTemplate jdbcTemplate;

    private static final RowMapper<Product> productRowMapper = (row, number) -> {
        Integer id = row.getInt("id");
        String category = row.getString("category");
        String title = row.getString("title");
        Double price = row.getDouble("price");
        Integer count_product = row.getInt("count_product");

        return new Product(id, category, title, price, count_product);
    };

    @Autowired
    public ProductRepository(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getCategory(), product.getTitle(), product.getPrice(), product.getCount_product());
    }

    private String sqlByPrice(double price) {
        String SQL_SELECT_PRICE = new String();
        SQL_SELECT_PRICE = "select * from product where cost > " + price;
        return SQL_SELECT_PRICE;
    }


    @Override
    public List<Product> findAllByPrice(double price) {
        String SQL_SELECT_PRICE = sqlByPrice(price);
        return jdbcTemplate.query(SQL_SELECT_PRICE, productRowMapper);

    }
}

