package ru.pcs.web.repositories;

import ru.pcs.web.models.Product;

import java.util.List;

/**
 * @author Pavlenko Artem
 */
public interface ShopClothingServices {
    List<Product> findAll();

    void save(Product product);

    List<Product> findAllByPrice(double price);


}
