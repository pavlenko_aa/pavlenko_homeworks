package ru.pcs.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pcs.web.models.Product;
import ru.pcs.web.repositories.ProductRepository;


/**
 * @author Pavlenko Artem
 */

@Controller
public class ProductController {
     @Autowired
    private ProductRepository productRepository;

    @PostMapping(value = "/products")
    public String addProduct(@RequestParam("category") String category,
                             @RequestParam("title") String title,
                             @RequestParam("price") Double price,
                             @RequestParam("count_product") Integer count_product) {

        Product product = Product.builder()
                .category(category)
                .title(title)
                .price(price)
                .count_product(count_product)
                .build();

        productRepository.save(product);
        return "redirect:/products.html";
    }

}
