package ru.pcs.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Pavlenko Artem
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Product {
    private Integer id;
    private String category;
    private String title;
    private Double price;
    private Integer count_product;

}
