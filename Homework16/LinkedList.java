package ru.pavlenko_homework16;

import org.w3c.dom.Node;

/**
 * @author Pavlenko Artem
 */
public class LinkedList<T> {

    private static class Node<T> {
        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> first;
    private Node<T> last;
    private int size;

    public void add(int element) {
        Node newNode = new Node(element);

        if (size == 0) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        size++;
    }

    public void addToBegin(T element) {
        Node newNode = new Node<>(element);
        if (size == 0) {
            last = newNode;
        } else {
            newNode.next = first;
        }
        first = newNode;
        size++;
    }

    public T get(int index){
       if(index >= 0 && index < size){
           Node current = first;
           for(int i = 0;i < index;i++) {
               current = current.next;
           }
           return (T) current.value;
       }
        System.out.println("Index uot of bounds");
       return null;
    }

    public int size() {
        return size;
    }

    @Override
    public String toString() {
        return "LinkedList{" +
                "first=" + first +
                ", last=" + last +
                ", size=" + size +
                '}';
    }
}
