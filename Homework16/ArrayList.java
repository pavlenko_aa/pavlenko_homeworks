package ru.pavlenko_homework16;

import java.util.Arrays;

public class ArrayList<T> {
    private static final int DEFAULT_SIZE = 10;

    private T[] elements;
    private int size;

    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_SIZE];
        this.size = 0;
    }

    public void add(T element) {
        if (isFullArray()) {
            resize();
        }
        this.elements[size] = element;
        size++;
    }

    private void resize() {
        T[] oldElements = this.elements;
        this.elements = (T[]) new Object[oldElements.length + oldElements.length / 2];
        for (int i = 0; i < size; i++) {
            this.elements[i] = oldElements[i];
        }
    }

    private boolean isFullArray() {
        return size == elements.length;
    }


    public void removeAt(int index) {
        if (index >= 0 && index < size) {
            System.arraycopy(elements, index + 1, elements, index, size - index - 1);
            size--;
        }
    }

    @Override
    public String toString() {
        return "ru.pavlenko_homework16.ArrayList{" +
                "elements=" + Arrays.toString(elements) +
                ", size=" + size +
                '}';
    }
}
