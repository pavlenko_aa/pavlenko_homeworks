import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int digitMax = 10;
        int digitMin = 0;
        while (num != -1) {
            while (num != 0) {
                int currentDigit = num % 10;
                num /= 10;
                if (currentDigit < digitMax) {
                    digitMin = currentDigit;
                    digitMax = digitMin;
                }
            }
            num = sc.nextInt();
        }
        System.out.println(digitMin);
    }
}
