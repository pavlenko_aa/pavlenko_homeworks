package ru.pavlenkoaa;

public class Main {

    public static void main(String[] args) {
        int[] arr = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        int n = arr.length;
        int result = getPositionDigit(arr, 18);
        System.out.println(result);
        getMoveArray(arr, n);
        printArray(arr,n);


    }
    public static int getPositionDigit(int[] array, int num) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == num) {
                return i;
            }
        }
        return -1;
    }
    public static void getMoveArray(int[] arr, int n) {
        int count = 0;
        int temp;
        for (int i = 0; i < n; i++) {
            if ((arr[i] != 0)) {
                temp = arr[count];
                arr[count] = arr[i];
                arr[i] = temp;
                count = count + 1;
            }
        }
    }
    static void printArray(int[] arr, int n) {
        for (int i = 0; i < n; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}

