package ru.pcs.web.forms;

import lombok.Data;

/**
 * 10.12.2021
 * AttestationWork
 *
 * @author Pavlenko Artem
 * @version v1.0
 */
@Data
public class SignUpForm {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
}
