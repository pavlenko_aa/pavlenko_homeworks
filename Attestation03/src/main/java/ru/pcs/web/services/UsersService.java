package ru.pcs.web.services;

import ru.pcs.web.forms.UserForm;
import ru.pcs.web.models.Product;
import ru.pcs.web.models.User;

import java.util.List;

/**
 * 10.12.2021
 * AttestationWork
 *
 * @author Pavlenko Artem
 * @version v1.0
 */
public interface UsersService {
    void addUser(UserForm form);
    List<User> getAllUsers();

    void deleteUser(Integer userId);

    User getUser(Integer userId);

    void addProductToUser(Integer userId, Integer productId);

    void update(Integer userId, UserForm userForm);
}
