package ru.pcs.web.services;

import ru.pcs.web.forms.SignUpForm;

/**
 * 10.12.2021
 * AttestationWork
 *
 * @author Pavlenko Artem
 * @version v1.0
 */
public interface SignUpService {
    void signUpUser(SignUpForm form);
}
