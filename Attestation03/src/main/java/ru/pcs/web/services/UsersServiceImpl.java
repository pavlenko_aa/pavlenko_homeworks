package ru.pcs.web.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.pcs.web.exceptions.UserNotFoundException;
import ru.pcs.web.forms.UserForm;
import ru.pcs.web.models.Product;
import ru.pcs.web.models.User;
import ru.pcs.web.repositories.ProductRepository;
import ru.pcs.web.repositories.UsersRepository;

import java.util.List;

/**
 * 10.12.2021
 * AttestationWork
 *
 * @author Pavlenko Artem
 * @version v1.0
 */
@RequiredArgsConstructor
@Component
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;
    private final ProductRepository productRepository;


    @Override
    public void addUser(UserForm form) {
        User user = User.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .build();

        usersRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public void deleteUser(Integer userId) {
        usersRepository.deleteById(userId);
    }

    @Override
    public User getUser(Integer userId) {
        return usersRepository.findById(userId).orElseThrow(UserNotFoundException::new);
    }


    @Override
    public void addProductToUser(Integer userId, Integer productId) {
        User user = usersRepository.getById(userId);
        Product product = productRepository.getById(productId);
        product.setOwner(user);
        productRepository.save(product);
    }

    @Override
    public void update(Integer userId, UserForm userForm) {
        User user = usersRepository.getById(userId);
        user.setFirstName(userForm.getFirstName());
        user.setLastName(userForm.getLastName());
        usersRepository.save(user);
    }
}
