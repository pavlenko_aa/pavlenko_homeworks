package ru.pcs.web.services;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * 10.12.2021
 * AttestationWork
 *
 * @author Pavlenko Artem
 * @version v1.0
 */
public interface FilesService {
    void saveFile(Integer ownerId, String description, MultipartFile multipartFile);

    void addFileToResponse(String fileName, HttpServletResponse response);
}
