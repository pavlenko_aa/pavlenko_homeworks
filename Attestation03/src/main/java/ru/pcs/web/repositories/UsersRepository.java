package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.User;

import java.util.Optional;

/**
 * 10.12.2021
 * AttestationWork
 *
 * @author Pavlenko Artem
 * @version v1.0
 */
public interface UsersRepository extends JpaRepository<User, Integer> {
    Optional<User> findByEmail(String email);
}
