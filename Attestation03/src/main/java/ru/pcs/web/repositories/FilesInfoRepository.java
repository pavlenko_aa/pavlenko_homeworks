package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.FileInfo;

/**
 * 10.12.2021
 * AttestationWork
 *
 * @author Pavlenko Artem
 * @version v1.0
 */
public interface FilesInfoRepository extends JpaRepository<FileInfo, Long> {
    FileInfo findByStorageName(String storageName);
}
