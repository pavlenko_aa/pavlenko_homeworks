package ru.pcs.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 10.12.2021
 * AttestationWork
 *
 * @author Pavlenko Artem
 * @version v1.0
 */
@Controller
@RequestMapping("/signIn")
public class SignInController {
    @GetMapping
    public String getSignInPage() {
        return "signIn";
    }
}
