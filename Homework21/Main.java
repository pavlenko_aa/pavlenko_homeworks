package ru.pavlenkoaa;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static int[] array;
    public static int[] sumsThread;


    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размерность массива млн. :");
        int numbersCount = scanner.nextInt();
        System.out.println("Введите колличество потоков :");
        int threadsCount = scanner.nextInt();

        array = new int[numbersCount];
        sumsThread = new int[threadsCount];

        // заполняем случайными числами
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }

        int realSum = 0;

        for (int i = 0; i < array.length; i++) {
            realSum += array[i];
        }

        // для 2 000 000 -> 98996497, 98913187
        System.out.println("Сумма без многопоточности : " + realSum);


        SumThread[] threads = new SumThread[threadsCount];

        int elementForSumInOneThread = numbersCount / threadsCount;

        for (int i = 0; i < threadsCount; i++) {
            if (i != threadsCount - 1) {
                threads[i] = new SumThread(i * elementForSumInOneThread, (i + 1) * elementForSumInOneThread - 1, i);
            } else {
                threads[i] = new SumThread(i * elementForSumInOneThread, numbersCount  - 1, i);
            }
            threads[i].start();
        }
        try {
            for (int i = 0; i < threadsCount; i++) {
                threads[i].join();
            }
        } catch (InterruptedException e) {
            throw new IllegalArgumentException(e);
        }

        int byThreadSum = 0;

        for (int i = 0; i < threadsCount; i++) {
            byThreadSum += sumsThread[i];
        }
        System.out.println("Сумма с многопоточностью : " + byThreadSum);
    }
}
