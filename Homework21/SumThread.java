package ru.pavlenkoaa;

/**
 * @author Pavlenko Artem
 */
public class SumThread extends Thread {
    private int from;
    private int to;
    private int threadNumber;


    public SumThread(int from, int to, int threadNumber) {
        this.from = from;
        this.to = to;
        this.threadNumber = threadNumber;
    }

    @Override
    public void run() {
        for (int i = from; i <= to; i++) {
            Main.sumsThread[threadNumber] += Main.array[i];
        }
    }
}
