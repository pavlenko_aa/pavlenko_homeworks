package ru.pavlenkoaa;


import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] array = {113,518,185,88,243,56};
        int[] resultFilterArray = Sequence.filter(array, number -> (number % 2 == 0));

        System.out.println(Arrays.toString(resultFilterArray));

        int[] resultArray = Sequence.filter(array,number -> {
            boolean result = false;
            int digitSum = 0;
            while (number != 0){
                int digit = number % 10;
                number = number / 10;
                digitSum += digit;
            }
            if(digitSum % 2 == 0){
                result = true;
            }
            return result;
        });
        System.out.println(Arrays.toString(resultArray));
    }
}

