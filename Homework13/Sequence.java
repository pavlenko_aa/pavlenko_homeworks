package ru.pavlenkoaa;

/**
 * @author Pavlenko Artem
 */
public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {
        int[] tempArray = new int[array.length];
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                tempArray[i] = array[i];
                count++;
            }
        }
        int i = 0;
        while (i < tempArray.length) {
            for (int j = i; j < tempArray.length - 1; j++) {
                int temp;
                if (tempArray[j] == 0) {
                    temp = tempArray[j];
                    tempArray[j] = tempArray[j + 1];
                    tempArray[j + 1] = temp;
                }
            }
            i++;
        }
        int[] result = new int[count];
        for (int k = 0; k < tempArray.length; k++) {
            if (tempArray[k] != 0) {
                result[k] = tempArray[k];
            }
        }
        return result;
    }
}
