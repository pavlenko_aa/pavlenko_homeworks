package ru.pcs;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;


@DisplayName(value = "NumbersUtil is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumbersUtilTest {

    private final NumbersUtil numbersUtil = new NumbersUtil();

    @Nested
    @DisplayName("isPrime() is working")
    public class ForIsPrime {

        @ParameterizedTest(name = "возвращает <true> on {0}")
        @ValueSource(ints = {2, 3, 71, 113})
        public void on_prime_numbers_return_true(int number) {
            assertTrue(numbersUtil.isPrime(number));
        }

        @ParameterizedTest(name = "возвращает <false> on {0}")
        @ValueSource(ints = {22, 33, 72, 114})
        public void on_not_prime_numbers_return_false(int number) {
            assertFalse(numbersUtil.isPrime(number));
        }

        @ParameterizedTest(name = "возвращает <false> on {0}")
        @ValueSource(ints = {121, 169})
        public void on_sqr_numbers_return_false(int sqrNumber) {
            assertFalse(numbersUtil.isPrime(sqrNumber));
        }

        @ParameterizedTest(name = "throw exception on {0}")
        @ValueSource(ints = {0, 1})
        public void bad_numbers_throws_exception(int badNumber) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.isPrime(badNumber));
        }
    }

    @Nested
    @DisplayName("gcd() is working")

    public class ForGcd {

        @ParameterizedTest(name = "возвращает НОД = {2} для {0} и {1}")
        @CsvSource(value = {"18,12,6", "9,12,3", "64,48,16"})
        public void return_correct_gcd(int a, int b, int nod) {
            assertEquals(nod, numbersUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "возвращает НОД = {2} для {0} и {1}")
        @CsvSource(value = {"5, 1, 1", "4, 1, 1", "11, 1, 1", "1,26,1"})
        public void positive_integer_number_with_1_return_true(int a, int b, int nod) {
            assertEquals(nod, numbersUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "throws exception on {0}")
        @ValueSource(ints = {0, -1})
        public void bad_gcd_throws_exception(int badGcd) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.gcd(badGcd, badGcd));
        }

    }
}