package ru.pcs;

/**
 * @author Pavlenko Artem
 */
public class Main {
    public static void main(String[] args) {
        NumbersUtil numbersUtil = new NumbersUtil();
        String msg = "НОД для чисел %d и %d равен %d";
        int a = 245;
        int b = 15;
        System.out.println(String.format(msg, a, b, numbersUtil.gcd(a, b)));

    }
}
