package ru.pcs;

/**
 * @author Pavlenko Artem
 */
public class NumbersUtil {


    public boolean isPrime(int number) {

        if (number == 0 || number == 1) {
            throw new IllegalArgumentException();
        }

        if (number == 2 || number == 3) {
            return true;
        }
        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public int gcd( int a, int b) {

        if(a <= 0 || b <= 0){
            throw new IllegalArgumentException();
        }

        if (a == 1 || b == 1) {
            return 1;
        }
        int temp;
        while (a != b) {
            temp = Math.min(a, b);
            b = Math.abs(a - b);
            a = temp;
        }
        return a;
    }
}

